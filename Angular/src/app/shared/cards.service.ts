export class CardsService {

constructor() { }
cards = [
	{ name: 'name1', title: 'title1', id: 'product1', photo: '', description: '1', holder: 'Garry', phone: '+1234567', category: 'category1'},
	{ name: 'name2', title: 'title2', id: 'product2', photo: '', description: '2', holder: 'Tom', phone: '+34685479', category: 'category2'},
	{ name: 'name3', title: 'title3', id: 'product3', photo: '', description: '3', holder: 'Helen', phone: '+7438095', category: 'category3'},
	{ name: 'name4', title: 'title4', id: 'product4', photo: '', description: '4', holder: 'Ammy', phone: '+6367934', category: 'category1'},
];

	public getCardById(id) {
		return this.cards.find(element => {
			return element.id === id;
		});
	}
}
