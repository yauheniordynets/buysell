import { Component } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// import { RegisterService } from '../shared/register.service';

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.scss' ]
})
export class RegistrationPageComponent implements OnInit  {

  registrationForm: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      nickname: [ '', Validators.required],
      phone: ['', Validators.required],
      password: ['', Validators.required],
      email: ['', Validators.email],
    });
  }

  onSubmit() {
    const controls = this.registrationForm.controls;
    if (this.registrationForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }
    console.log(this.registrationForm.value);
  }
}