import { Component, OnInit, Input } from '@angular/core';
import { CardsService } from '../shared/cards.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
	selector: 'app-product-page',
	templateUrl: './product-page.component.html',
	styleUrls: ['./product-page.component.scss'],
	providers: [CardsService]
})

export class ProductPageComponent implements OnInit {
	cardInfo;
	
	constructor(private route: ActivatedRoute, private service: CardsService) { }

	ngOnInit() {
		this.route.params.subscribe(params => {
			this.cardInfo = this.service.getCardById(params['id']);
		});
	}
}


