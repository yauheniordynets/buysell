import { Component } from '@angular/core';

import { CardsService } from './shared/cards.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CardsService]
})
export class AppComponent {
}
