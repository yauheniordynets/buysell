import { Component, OnInit } from '@angular/core';
import { CardsService } from '../shared/cards.service';

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page-component.scss']
})

export class HomePageComponent implements OnInit {
    cards = [];
    filteredCards = [];
    allNewCards = [];
    unsortCards = [];
    isSort = true;

    constructor(private cardsService: CardsService) { }

    ngOnInit() {
        this.cards = this.cardsService.cards;

    };

    Filter(value) {
        if (this.isSort){
            this.filteredCards = this.cards
            this.cards = this.cardsService.cards.filter(function (element) {
                return element.category === value;
            });
            this.isSort = false;
            this.allNewCards = this.filteredCards.filter(element => element.category !== value);
        } else {
            this.isSort = true;
            this.unsortCards = this.cards.concat(this.allNewCards);
            this.cards = this.unsortCards;
        }
    }
}
