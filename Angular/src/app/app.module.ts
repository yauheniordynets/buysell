import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppComponent } from './app.component';
import { CardsComponent } from './cards/cards.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RegistrationPageComponent } from './registration-page/registration-page.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductComponent } from './product/product.component';
import { ProductPageComponent } from './product-page/product-page.component';
import { NewCardComponent } from './new-card/new-card.component';

const routes = [
    { path: '', component: HomePageComponent },
    { path: 'register', component: RegistrationPageComponent },
    { path: 'newcard', component: NewCardComponent },
    { path: ':id', component: ProductPageComponent }

];

@NgModule({
    declarations: [
        AppComponent,
        CardsComponent,
        HomePageComponent,
        RegistrationPageComponent,
        ProductComponent,
        ProductPageComponent,
        NewCardComponent
    ],
    imports: [
        BrowserModule, 
        RouterModule.forRoot(routes),       
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
