﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiProj.Models;

namespace ApiProj.Controllers
{
    [Produces("application/json")]
    [Route("api/Adverts")]
    public class AdvertsController : Controller
    {
        private readonly ApplicationContext _context;

        public AdvertsController(ApplicationContext context)
        {
            _context = context;
            if (!_context.Adverts.Any())
            {
                _context.Adverts.Add(new Advert { Title = "Title1", Content = "Content1" });
                _context.SaveChanges();
            }
        }

        // GET: api/Adverts
        [HttpGet]
        public IEnumerable<Advert> GetAdverts()
        {
            return _context.Adverts;
        }

        // GET: api/Adverts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAdvert([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var advert = await _context.Adverts.SingleOrDefaultAsync(m => m.Id == id);

            if (advert == null)
            {
                return NotFound();
            }

            return Ok(advert);
        }

        // PUT: api/Adverts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAdvert([FromRoute] int id, [FromBody] Advert advert)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != advert.Id)
            {
                return BadRequest();
            }

            _context.Entry(advert).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AdvertExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Adverts
        [HttpPost]
        public async Task<IActionResult> PostAdvert([FromBody] Advert advert)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Adverts.Add(advert);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAdvert", new { id = advert.Id }, advert);
        }

        // DELETE: api/Adverts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAdvert([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var advert = await _context.Adverts.SingleOrDefaultAsync(m => m.Id == id);
            if (advert == null)
            {
                return NotFound();
            }

            _context.Adverts.Remove(advert);
            await _context.SaveChangesAsync();

            return Ok(advert);
        }

        private bool AdvertExists(int id)
        {
            return _context.Adverts.Any(e => e.Id == id);
        }
    }
}